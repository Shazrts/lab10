
(define Addition(lambda (x y)         
		(Success x (Success y 0)) ))
		
(define Subtraction(lambda (x y)         
		(Pred y x) ))			
		 
(define AND(lambda (M N)         
		 (N (M "T" "F") "F")))		
		 
(define NOT(lambda (M)        
		 (M "F" "T")))

(define Great_n_equal(lambda (x y)       
		(if( zero? (Subtraction y x))
		"T"
		"F"
		)))

(define Less_n_equal(lambda (x y)    
   
		(if( zero? (Subtraction x y))
		"T"
		"F"
		)))

		 
(define (Pred n z)
	  (define (iter n z)
		(if (zero? z)
			z
			(if (zero? n)
				z
				(iter (- n 1) (- z 1)))))
	  (iter n z))		
		
		
(define (Success n z)
	  (define (iter n z)
	    (if (zero? n)
	        z
	        (iter (- n 1) (+ z 1))))
	  (iter n z))
	  
(define T(lambda (x y)         
		 x ))
		 
(define F(lambda (x y)         
		 y ))